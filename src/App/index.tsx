import React from "react";
import GlobalStyle from "../Styles/GlobalStyle";
import { Container } from "./style";
import SideBar from "../Components/SideBar";
import Content from "../Components/Content";


function App() {
  return (
    <>
      <GlobalStyle />
        <Container>
            {/* Mobile Menu Bar */}
            
            {/* SideBar */}
            <SideBar></SideBar>

            {/* Content */}
            <Content></Content>

        </Container>
     
    </>
  );
}

export default App;
