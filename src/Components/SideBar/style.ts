import tw, { styled } from "twin.macro";

/* export const Container = styled.div`
    ${tw`bg-blue-800 text-blue-100 w-64 space-y-6 px-2 py-8 absolute inset-y-0 left-0 transform -translate-x-full transition duration-200 ease-in-out md:relative md:translate-x-0`}
`; */

type Props = {
  showMenu: boolean;
};

export const Container = styled.div(({ showMenu }: Props) => [
  tw`bg-blue-800 text-blue-100 w-64 space-y-6 px-2 py-8 absolute inset-y-0 left-0 transform  transition duration-200 ease-in-out md:relative md:translate-x-0`,

  showMenu && tw`-translate-x-full`,
]);
