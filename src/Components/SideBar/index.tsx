import React, { useState } from "react";

import { Container } from "./style";

import tw, { css, styled } from "twin.macro";



const SideBar: React.FC = () => {

  const [menu, setMenu] = useState<boolean>(true)
  return (
    <>
      <div css={[tw`bg-gray-800 text-gray-100 flex justify-between md:hidden`]}>
        {/* Logo */}
        <a
          href="#"
          css={[tw`no-underline text-white flex items-center space-x-2 px-4`]}
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            css={[tw`w-8 w-8 text-blue-100`]}
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
          >
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10.325 4.317c.426-1.756 2.924-1.756 3.35 0a1.724 1.724 0 002.573 1.066c1.543-.94 3.31.826 2.37 2.37a1.724 1.724 0 001.065 2.572c1.756.426 1.756 2.924 0 3.35a1.724 1.724 0 00-1.066 2.573c.94 1.543-.826 3.31-2.37 2.37a1.724 1.724 0 00-2.572 1.065c-.426 1.756-2.924 1.756-3.35 0a1.724 1.724 0 00-2.573-1.066c-1.543.94-3.31-.826-2.37-2.37a1.724 1.724 0 00-1.065-2.572c-1.756-.426-1.756-2.924 0-3.35a1.724 1.724 0 001.066-2.573c-.94-1.543.826-3.31 2.37-2.37.996.608 2.296.07 2.572-1.065z" />
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 12a3 3 0 11-6 0 3 3 0 016 0z" />
          </svg>
          <span css={[tw`text-2xl font-extrabold`]}>Better DevOps</span>
        </a>

        {/* Button */}
        <button css={[tw`bg-transparent text-gray-100 p-4 focus:outline-none focus:bg-gray-600`]} onClick={() => setMenu(p => p = !p)}>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            css={[tw`h-5 w-5`]}
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
          >
            <path
              stroke-linecap="round"
              stroke-linejoin="round"
              stroke-width="2"
              d="M4 6h16M4 12h16M4 18h16"
            />
          </svg>
        </button>
      </div>
      <Container showMenu={menu}>
        {/* Logo */}
        <a
          href="#"
          css={[tw`no-underline text-white flex items-center space-x-2 px-4`]}
        >
          <svg xmlns="http://www.w3.org/2000/svg" css={[tw`w-8 w-8 text-blue-100`]} fill="none" viewBox="0 0 24 24" stroke="currentColor">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10.325 4.317c.426-1.756 2.924-1.756 3.35 0a1.724 1.724 0 002.573 1.066c1.543-.94 3.31.826 2.37 2.37a1.724 1.724 0 001.065 2.572c1.756.426 1.756 2.924 0 3.35a1.724 1.724 0 00-1.066 2.573c.94 1.543-.826 3.31-2.37 2.37a1.724 1.724 0 00-2.572 1.065c-.426 1.756-2.924 1.756-3.35 0a1.724 1.724 0 00-2.573-1.066c-1.543.94-3.31-.826-2.37-2.37a1.724 1.724 0 00-1.065-2.572c-1.756-.426-1.756-2.924 0-3.35a1.724 1.724 0 001.066-2.573c-.94-1.543.826-3.31 2.37-2.37.996.608 2.296.07 2.572-1.065z" />
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 12a3 3 0 11-6 0 3 3 0 016 0z" />
          </svg>
          <span css={[tw`text-xl font-extrabold`]}>Better DevOps</span>
        </a>
        {/* nav */}
        <nav>
          <a
            href=""
            css={[
              tw`block py-2.5 px-4 no-underline text-white rounded hover:bg-red-300 transition duration-200`,
            ]}
          >
            Home
          </a>
          <a
            href=""
            css={[
              tw`block py-2.5 px-4 no-underline text-white rounded hover:bg-red-300 transition duration-200`,
            ]}
          >
            About
          </a>
          <a
            href=""
            css={[
              tw`block py-2.5 px-4 no-underline text-white rounded hover:bg-red-300 transition duration-200`,
            ]}
          >
            Features
          </a>
          <a
            href=""
            css={[
              tw`block py-2.5 px-4 no-underline text-white rounded hover:bg-red-300 transition duration-200`,
            ]}
          >
            Pricing
          </a>
        </nav>
      </Container>
    </>
  );
};

export default SideBar;
