import tw, { styled } from 'twin.macro';

export const Container = styled.div`
    ${tw`flex-1 p-10 text-2xl font-bold`}
`;

