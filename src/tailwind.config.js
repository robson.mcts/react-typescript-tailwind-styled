module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    container: {
      center: true,
      padding: '2rem'
    },
    extend: {
      colors: {
        primary: "var(--color-primary)", // new
        secondary: "var(--color-secondary)", // new
        whiteAlt: "var(--color-white-alt)", // new
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
