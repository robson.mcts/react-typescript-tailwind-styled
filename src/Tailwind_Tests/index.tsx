import React from "react";
import GlobalStyle from "../Styles/GlobalStyle";
import { StyledApp } from "./style";
import tw, { styled, css } from "twin.macro";

const Heading = styled.h1`
  ${tw`text-red-800`};
`;

const Container = styled.div`
  ${tw` min-h-full min-w-full bg-gray-50`}
`;

const Button = styled.button`
  ${tw`bg-blue-500 text-white font-bold py-2 px-4 rounded ml-0 mr-1 hover:bg-blue-400 duration-100`}
`;

const Block = styled.div`
  ${tw`block md:flex align-`}
`;

const InnerBlock = styled.div`
  ${tw`bg-blue-700 py-24 px-8`}
`;

function App() {
  return (
    <>
      <GlobalStyle />

      <Container>
        <Block>
          <InnerBlock>
            <h1 css={[tw`text-blue-300 text-center`]}>The TailWindCSS Tutorial</h1>
            <p css={[tw`text-white text-center` ]}>From Gary Simon</p>
          </InnerBlock>
          <div css={[tw`py-12 px-8`]}>
            <div css={[tw`bg-white inline-flex p-6 rounded-lg shadow-md`]}>
              <div css={[tw`bg-blue-400 rounded-full w-12 h-12`]}></div>
              <p css={[tw`mt-3 ml-4 `]}>It is truly awesome</p>
            </div>
          </div>
        </Block>
      </Container>
    </>
  );
}

export default App;
