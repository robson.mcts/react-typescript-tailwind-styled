# Tailwind Reactjs App

Demo app using ReactJs, Tailwind 2.0, TypeScript and StyledComponents

## Live 
https://tailwind.robsoncarvalho.com.br

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `docker-compose up --build`

Runs the app in a docker container.
Open  [http://localhost](http://localhost) to view it in the browser.

### `docker build -t myreactimage:latest .`
Build a local docker image named myreactimage:latest

### `docker run -p 80:80 --name myreactapp myreactimage:latest .`
Run a docker container app interactivily from locally stored docker image myreactimage

# Deploying the docker image on Amazon ECS Cluster

1. Install AWS CLI and configure following the AWS docs below:

    https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2-linux.html
    https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-configure.html

2. Create a new Elastic Container Registry (ECR)
    
    `aws ecr create-repository --repository-name my-react-app --region eu-west-1`

3. Open AWS Console > Elastic Container Registry 

    https://console.aws.amazon.com/ecr/repositories?region=eu-west-1

4. Select the repository and then Click View push commands 

5. Copy all commands from the modal and past into the terminal - Don't use the snippet below - It won't work on your environment 

        aws ecr get-login-password --region eu-west-1 | docker login --username AWS --password-stdin 000000000000.dkr.ecr.eu-west-1.amazonaws.com
        
        docker build -t my-react-app .
        
        docker tag my-react-app:latest 000000000000.dkr.ecr.eu-west-1.amazonaws.com/my-react-app:latest
        
        docker push 000000000000.dkr.ecr.eu-west-1.amazonaws.com/my-react-app:latest

6. Once your image is copied to the ECR repository, copy its URI and past it in some note pad or text file

7. Go to AWS Console > Elastic Container Service
    
    https://console.aws.amazon.com/ecs/home?region=eu-west-1#/clusters

8. Creating and configuring our task definition

    Select Task Definitions > Click on Create new Task Definition 
        
        a. For Launch Type select Fargate and click Next Step
        b. On Configure Task and container definitions 
            - Task Definition Name:  my-react-app
            - Task Role: none
            - Task memory (GB): 0.5GB
            - Task CPU (vCPU): 0.25 vCPU
            
        c. Click on Add container
            - Container Name: my-react-app
            - Image: The URI from step 6
                000000000000.dkr.ecr.eu-west-1.amazonaws.com/my-react-app:latest
            - Memory Limits (MiB): Soft limit 128
            - Port mappings: 80
        d. Click on Add to add a container, and then click on Create to create the task definition.

9. Now it is time to create our cluster, so we can run our application in the AWS. 

        a. Go to Cluster page and click on Create Cluster
            https://console.aws.amazon.com/ecs/home?region=eu-west-1#/clusters 
        b. On the cluster template, select Network only and then click Next step
        c. Give a name for your cluster and then click Create
        d. Click View Cluster

10. To run our application, we need to click on the Tasks Tab and then click on Run new Task
    
     Complete the Task Run page as follows
    
        a. Launch Type: FARGATE
        b. Cluster VPC: Select one VPC
        c. Subnet: Select one subnet
        d. Security Group: You can select one or leave the default option
        e. Click Run Task

11. Wait till the Last status changes into RUNNING
12. Once the task status is running, click on the task name and check the Public IP address in the Network section.

More info:
    https://aws.amazon.com/getting-started/hands-on/deploy-docker-containers/
